function concatenateIf() {
  // Accepts an unlimited number of pairs of values. The first value
  // in each pair is a string. The second value is a boolean. The string
  // is concatenated to the final string depending on the value of its
  // corresponding boolean value. A space is automatically added between
  // the strings.
  // Ex: =concatenateIf("foo", true, "bar", false, "tree", true)
  // Output: foo tree

  var args = [];
  var result = "";
  for (var i = 0; i < arguments.length; ++i) args[i] = arguments[i];

  for (var i = 0; i < arguments.length; i += 2)
  {
    if( arguments[ i + 1 ] )
    {
      if ( result.length > 0 )
        result = result.concat( " " );

      result = result.concat( arguments[i] );
    }
  }

  return result;
}
